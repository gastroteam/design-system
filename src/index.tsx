import React from "react"
import ReactDOM from "react-dom/client"
import "./index.css"
import { AlertIcon } from "./ui/icons/alert"
import { NavLink } from "./ui/nav-links/nav-link"
import { CheckIcon } from "./ui/icons/check"
import { SettingsIcon } from "./ui/icons/settings"
import { Span, typography } from "./ui/typography"
import { colors } from "./ui/colors"
import { Settings } from "./ui/nav-links/settings"
import { Home } from "./ui/nav-links/home"
import { NewEntry } from "./ui/nav-links/new-entry"
import { Profile } from "./ui/nav-links/profile"

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement)
root.render(
  <React.StrictMode>
    {/* <NavLink
      icon={<SettingsIcon size={24} />}
      title="Настройки"
      isActive={false}
    />
    <NavLink icon={<CheckIcon />} title="Проверить" isActive={true} />
    <NavLink icon={<AlertIcon />} title="Внимание" isActive={false} />
    <Span
      color={colors.text.accent}
      size={typography.sizes.huge}
      family={typography.families.NeueMachina}
    >
      Some Text
    </Span> */}

    <Home size={typography.sizes.large} />
    <NewEntry size={typography.sizes.large} />
    <Settings size={typography.sizes.huge} />
    <Profile size={typography.sizes.huge} />
  </React.StrictMode>
)
