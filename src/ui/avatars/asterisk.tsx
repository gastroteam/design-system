import { IIconProps } from "../../../types";

export const Asterisk = ({ color = "currentColor", size = 40 }: IIconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
    >
      <g clip-path="url(#clip0_820_3132)">
        <circle
          cx={size / 2}
          cy={size / 2}
          r={(size / 2) * 0.9}
          fill="transparent"
          stroke={color}
          stroke-width="2"
        />
        <path
          d="M18.0695 29.3258V24.6516L14.7633 27.9577L11.457 31.2638L10.0966 29.9034L8.73617 28.543L12.0423 25.2367L15.3484 21.9305H10.6742H6V20V18.0695H10.6742H15.3484L12.0423 14.7633L8.73617 11.457L10.0966 10.0966L11.457 8.73616L14.7633 12.0423L18.0695 15.3484V10.6742V6H20H21.9305V10.6742V15.3484L25.2367 12.0423L28.543 8.73616L29.9034 10.0966L31.2638 11.457L27.9577 14.7633L24.6516 18.0695H29.3258H34V20V21.9305H29.3258H24.6516L27.9577 25.2367L31.2638 28.543L29.9034 29.9034L28.543 31.2638L25.2367 27.9577L21.9305 24.6516V29.3258V34H20H18.0695V29.3258Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0_820_3132">
          <rect width={size} height={size} fill="transparent" />
        </clipPath>
      </defs>
    </svg>
  );
};
