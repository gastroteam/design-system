import { IIconProps } from "../../../types";

export const Cook = ({ color = "currentColor", size = 40 }: IIconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
    >
      <g clip-path="url(#clip0_107_484)">
        <circle
          cx={size / 2}
          cy={size / 2}
          r={(size / 2) * 0.9}
          fill="transparent"
          stroke={color}
          stroke-width="2"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M25.0715 10.7489C25.673 10.5374 26.3018 10.4317 26.9853 10.4317C30.2934 10.4317 32.9727 13.022 33 16.2203C33 19.1013 30.8128 21.4802 27.9695 21.9295V26H12.0305V21.9295C9.18717 21.4802 7 19.1013 7 16.2203C7 13.022 9.67928 10.4317 12.9874 10.4317C13.6435 10.4317 14.2997 10.5374 14.9012 10.7489C15.9674 9.11013 17.8265 8 19.9863 8C22.1462 8 24.0053 9.11013 25.0715 10.7489ZM12 30.9934V28H28V30.9934C28 31.5497 27.5334 32 26.9571 32H13.0429C12.4666 32 12 31.5497 12 30.9934Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0_107_484">
          <rect width={size} height={size} fill="transparent" />
        </clipPath>
      </defs>
    </svg>
  );
};
