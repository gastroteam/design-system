import { IIconProps } from "../../../types";

export const Human = ({ color = "currentColor", size = 40 }: IIconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
    >
      <g clip-path="url(#clip0_820_3124)">
        <circle
          cx={size / 2}
          cy={size / 2}
          r={(size / 2) * 0.9}
          fill="transparent"
          stroke={color}
          stroke-width="2"
        />
        <path
          d="M6.99995 25C8.49954 29.9987 14.0019 33.9978 20 34C25.998 33.9978 31.5004 29.9987 33 25C34.068 21.4403 29.2536 20.7087 26.8433 20.1877C26.3041 20.0708 25.7412 20.1745 25.2573 20.4395C24.1822 21.0283 22.0911 22 20 22C17.9089 22 15.8178 21.0283 14.7427 20.4395C14.2588 20.1745 13.6959 20.0708 13.1567 20.1877C10.7464 20.7087 5.93203 21.4403 6.99995 25Z"
          fill={color}
        />
        <path
          d="M14 12.017C14 15.3144 16.7119 18 20 18C23.2881 18 26 15.3144 26 12.017C26 8.68556 23.2881 6.00002 20 6C16.7119 6.00001 14 8.68556 14 12.017Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0_820_3124">
          <rect width={size} height={size} fill="transparent" />
        </clipPath>
      </defs>
    </svg>
  );
};
