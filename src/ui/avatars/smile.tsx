import { IIconProps } from "../../../types";

export const Smile = ({ color = "currentColor", size = 40 }: IIconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
    >
      <g clip-path="url(#clip0_20_1669)">
        <circle
          cx={size / 2}
          cy={size / 2}
          r={(size / 2) * 0.9}
          fill="transparent"
          stroke={color}
          stroke-width="2"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M16 14C16 15.6569 14.6569 17 13 17C11.3431 17 10 15.6569 10 14C10 12.3431 11.3431 11 13 11C14.6569 11 16 12.3431 16 14ZM30 14C30 15.6569 28.6569 17 27 17C25.3431 17 24 15.6569 24 14C24 12.3431 25.3431 11 27 11C28.6569 11 30 12.3431 30 14ZM9.30739 22.2646C8.90124 21.5426 7.98667 21.2865 7.26463 21.6927C6.54259 22.0988 6.28651 23.0134 6.69266 23.7354C9.90566 29.4474 14.8586 32.5 20 32.5C25.1415 32.5 30.0944 29.4474 33.3074 23.7354C33.7135 23.0134 33.4575 22.0988 32.7354 21.6927C32.0134 21.2865 31.0988 21.5426 30.6927 22.2646C27.9057 27.2193 23.8586 29.5 20 29.5C16.1415 29.5 12.0944 27.2193 9.30739 22.2646Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0_20_1669">
          <rect width={size} height={size} fill="transparent" />
        </clipPath>
      </defs>
    </svg>
  );
};
