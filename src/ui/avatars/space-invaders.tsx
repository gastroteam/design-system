import { IIconProps } from "../../../types";

export const SpaceInvaders = ({
  color = "currentColor",
  size = 40,
}: IIconProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox={`0 0 ${size} ${size}`}
      fill="none"
    >
      <g clip-path="url(#clip0_107_429)">
        <circle
          cx={size / 2}
          cy={size / 2}
          r={(size / 2) * 0.9}
          fill="transparent"
          stroke={color}
          stroke-width="2"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M10 9H13V12H10V9ZM27 12H24V15H21H19H16V12L13 12V15H10V17H8L8 15V12H5V15V17V20V23H8V25H10V28H8V31H10V28H13V25H16H19H21H24H27V28H30V31H32V28H30V25H32V23H35V20V17V15V12H32V15V17H30V15H27V12ZM27 12V9H30V12L27 12ZM27 20V17H24V20H27ZM16 17V20H13V17H16Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0_107_429">
          <rect width={size} height={size} fill="transparent" />
        </clipPath>
      </defs>
    </svg>
  );
};
