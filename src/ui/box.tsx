export const getShift = (val: number, spacing: number): number => {
  return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].indexOf(val) > -1
    ? val * spacing
    : 0;
};

interface IGetBoxStyles {
  p?: {
    m?: number;
    mt?: number;
    mb?: number;
    ml?: number;
    mr?: number;

    p?: number;
    pt?: number;
    pb?: number;
    pl?: number;
    pr?: number;
  };
  spacing?: number;
}

const defaultValue = {
  m: 0,
  mt: 0,
  mb: 0,
  ml: 0,
  mr: 0,

  p: 0,
  pt: 0,
  pb: 0,
  pl: 0,
  pr: 0,
};

export const getBoxStyles = ({
  p = defaultValue,
  spacing = 0,
}: IGetBoxStyles): string => {
  const shiftStyles = [];

  // margin
  if (typeof p?.m !== "undefined") {
    shiftStyles.push(`margin: ${getShift(p.m, spacing)}px;`);
  }
  if (typeof p?.mt !== "undefined") {
    shiftStyles.push(`margin-top: ${getShift(p.mt, spacing)}px;`);
  }
  if (typeof p?.mb !== "undefined") {
    shiftStyles.push(`margin-bottom: ${getShift(p.mb, spacing)}px;`);
  }
  if (typeof p?.ml !== "undefined") {
    shiftStyles.push(`margin-left: ${getShift(p.ml, spacing)}px;`);
  }
  if (typeof p?.mr !== "undefined") {
    shiftStyles.push(`margin-right: ${getShift(p.mr, spacing)}px;`);
  }

  // padding
  if (typeof p?.p !== "undefined") {
    shiftStyles.push(`padding: ${getShift(p.p, spacing)}px;`);
  }
  if (typeof p?.pt !== "undefined") {
    shiftStyles.push(`padding-top: ${getShift(p.pt, spacing)}px;`);
  }
  if (typeof p?.pb !== "undefined") {
    shiftStyles.push(`padding-bottom: ${getShift(p.pb, spacing)}px;`);
  }
  if (typeof p?.pl !== "undefined") {
    shiftStyles.push(`padding-left: ${getShift(p.pl, spacing)}px;`);
  }
  if (typeof p?.pr !== "undefined") {
    shiftStyles.push(`padding-right: ${getShift(p.pr, spacing)}px;`);
  }

  return shiftStyles.join(" ");
};
