export const colors = {
  text: {
    transparent: "transparent",
    primary: "rgba(10, 10, 11, 1)",
    secondary: "rgba(98, 98, 106, 1)",
    accent: "rgba(0, 0, 255, 1)",
    disabled: "rgba(149, 149, 157, 1)",
  },
  input: {
    accent: {
      border: "rgba(0, 0, 255, 1)",
    },
    success: {
      border: "rgba(11, 218, 98, 1)",
      description: "Изменения сохранены!",
    },
    alert: {
      border: "rgba(243, 32, 12, 1)",
      description: "Ошибка!",
    },
  },
};
