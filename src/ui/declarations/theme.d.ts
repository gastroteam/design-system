import { ILayout } from "../types/layout";
import { IColorText } from "../types/palette-color.enum";
import { ITextBlock } from "../typography";

declare module "styled-components" {
  export interface DefaultTheme {
    layout: ILayout;
    buttons: ButtonSettings;
    colorsText: IColorText;
    colorsBackground: IColorBackground;
    typography: ITextBlock;
  }
}
