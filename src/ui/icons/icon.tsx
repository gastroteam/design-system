import styled, { css } from "styled-components";

interface IIconWrapperProps {
  children: JSX.Element;
}

const Icon = styled.div`
  display: flex;
`;

export const IconWrapper = ({ children }: IIconWrapperProps) => {
  return <Icon>{children}</Icon>;
};
