export const iconColors = {
  default: "rgba(10, 10, 11, 1)",
  secondary: "rgba(98, 98, 106, 1)",
  accent: "rgba(0, 0, 255, 1)",
  success: "rgba(11, 218, 98, 1)",
  alert: "rgba(243, 32, 12, 1)",
};
