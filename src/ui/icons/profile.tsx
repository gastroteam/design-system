import { IIconProps } from "../../../types"

export const ProfileIcon = ({
  color = "currentColor",
  size = 24,
}: IIconProps): React.JSX.Element => {
  return (
    <svg
      style={{ display: "flex" }}
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox="0 0 24 24"
      fill="none"
    >
      <g clip-path="url(#clip0_20_1958)">
        <circle
          cx="12"
          cy="12"
          r="11"
          fill="transparent"
          stroke={color}
          stroke-width="2"
        />
        <path
          d="M4.20002 15.0001C5.09978 17.9993 8.40121 20.3988 12 20.4001C15.5989 20.3988 18.9003 17.9993 19.8001 15.0001C20.4408 12.8643 17.5522 12.4253 16.106 12.1127C15.7825 12.0426 15.4447 12.1048 15.1544 12.2638C14.5093 12.6171 13.2547 13.2001 12 13.2001C10.7454 13.2001 9.49074 12.6171 8.84567 12.2638C8.55534 12.1048 8.21759 12.0426 7.89408 12.1127C6.44788 12.4253 3.55927 12.8643 4.20002 15.0001Z"
          fill={color}
        />
        <path
          d="M8.40006 7.2103C8.40006 9.18876 10.0272 10.8001 12 10.8001C13.9729 10.8001 15.6 9.18876 15.6 7.2103C15.6 5.21143 13.9729 3.60011 12 3.6001C10.0272 3.60011 8.40006 5.21143 8.40006 7.2103Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0_20_1958">
          <rect width={size} height={size} fill="white" />
        </clipPath>
      </defs>
    </svg>
  )
}
