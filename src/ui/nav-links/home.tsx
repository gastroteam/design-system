import { colors } from "../colors"
import { HomeIcon } from "../icons/home"
import { typography } from "../typography"
import { NavLink } from "./nav-link"

interface IHomeProps {
  size: typeof typography.sizes.default
  title?: string
}

export const Home = ({
  size,
  title = "Главная",
}: IHomeProps): React.JSX.Element => {
  return (
    <NavLink
      icon={<HomeIcon size={size.lineHeight} />}
      title={title}
      size={size}
      family={typography.families.Lucida}
      color={colors.text.secondary}
      isActive
    ></NavLink>
  )
}
