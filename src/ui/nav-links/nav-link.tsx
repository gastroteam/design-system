import styled from "styled-components"
import { Span, typography } from "../typography"
import { colors } from "../colors"
import React from "react"

interface INavLinkProps {
  icon: React.JSX.Element
  title?: string
  size?: typeof typography.sizes.default
  family?: typeof typography.families.Lucida
  color?: typeof colors.text.secondary
  isActive: boolean
}

const Li = styled.li<{
  $isActive?: boolean
}>`
  margin: 0;
  padding: 8px 16px;
  border: 0;
  border-radius: 8px;
  gap: 8px;
  text-decoration: none;
  vertical-align: baseline;

  display: flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  /* transition: color 0.2s ease-in-out; */

  &:hover {
    color: ${colors.text.primary};
    fill: ${colors.text.primary};

    & > span {
      color: ${colors.text.primary};
      fill: ${colors.text.primary};

      /* transition: color 0.2s ease-in-out; */
    }
  }

  &:active {
    color: ${colors.text.accent};
    fill: ${colors.text.accent};

    & > span {
      color: ${colors.text.accent};
      fill: ${colors.text.accent};

      /* transition: color 0.2s ease-in-out; */
    }
  }

  color: ${(props) =>
    props.$isActive ? colors.text.accent : colors.text.secondary};
  fill: ${(props) =>
    props.$isActive ? colors.text.accent : colors.text.secondary};

  & > span {
    color: ${(props) =>
      props.$isActive ? colors.text.accent : colors.text.secondary};
  }
  & > span {
    fill: ${(props) =>
      props.$isActive ? colors.text.accent : colors.text.secondary};
  }
`

export const NavLink = ({
  icon,
  title = "",
  size = typography.sizes.default,
  family = typography.families.Lucida,
  color = colors.text.secondary,
  isActive,
}: INavLinkProps): React.JSX.Element => {
  return (
    <Li $isActive={isActive}>
      {icon}
      <Span size={size} family={family} color={color}>
        {title}
      </Span>
    </Li>
  )
}
