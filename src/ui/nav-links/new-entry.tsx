import { colors } from "../colors"
import { EditIcon } from "../icons/edit"
import { typography } from "../typography"
import { NavLink } from "./nav-link"

interface INewEntryProps {
  size: typeof typography.sizes.default
  title?: string
}

export const NewEntry = ({
  size,
  title = "Новая запись",
}: INewEntryProps): React.JSX.Element => {
  return (
    <NavLink
      icon={<EditIcon size={size.lineHeight} />}
      title={title}
      size={size}
      family={typography.families.Lucida}
      color={colors.text.secondary}
      isActive={false}
    ></NavLink>
  )
}
