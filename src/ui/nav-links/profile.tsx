import { colors } from "../colors"
import { ProfileIcon } from "../icons/profile"
import { typography } from "../typography"
import { NavLink } from "./nav-link"

interface IProfileProps {
  size: typeof typography.sizes.default
  title?: string
}

export const Profile = ({
  size,
  title = "Username",
}: IProfileProps): React.JSX.Element => {
  return (
    <NavLink
      icon={<ProfileIcon size={size.lineHeight} />}
      title={title}
      size={size}
      family={typography.families.Lucida}
      color={colors.text.secondary}
      isActive={false}
    ></NavLink>
  )
}
