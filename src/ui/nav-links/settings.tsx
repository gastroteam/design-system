import { colors } from "../colors"
import { SettingsIcon } from "../icons/settings"
import { typography } from "../typography"
import { NavLink } from "./nav-link"

interface ISettingsProps {
  size: typeof typography.sizes.default
  title?: string
}

export const Settings = ({
  size,
  title = "Настройки",
}: ISettingsProps): React.JSX.Element => {
  return (
    <NavLink
      icon={<SettingsIcon size={size.lineHeight} />}
      title={title}
      size={size}
      family={typography.families.Lucida}
      color={colors.text.secondary}
      isActive
    ></NavLink>
  )
}
