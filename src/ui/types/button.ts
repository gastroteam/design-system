import { IColorBackground, IColorText } from "./palette-color.enum";

export interface ButtonSizeStyle {
  height: number;
  paddingVertical: number;
  paddingHorizontal: number;
  fontSize: number;
  fontWeight: number;
  borderWidth?: number;
  borderRadius?: number;
}
export interface ButtonStyle {
  borderColor?: IColorBackground;
  color: IColorText;
  backgroundColor?: IColorBackground;
}
