// Палитра цветов для текста
export enum PaletteColorText {
  Transparent = "transparent",
  TextPrimary = "oklch(14.52% 0.002 286.13)", // rgba(10, 10, 11, 1)
  TextSecondary = "oklch(49.89% 0.013 285.93)", // rgba(98, 98, 106, 1)
  TextAccent = "oklch(45.2% 0.2655745880837794 264.052020638055)", // rgba(0, 0, 255, 1)
  TextDisabled = "oklch(67.24% 0.012 286.08)", // rgba(149, 149, 157, 1)
  TextSuccess = "oklch(77.64% 0.216 149.22)", // rgba(11, 218, 98, 1)
  TextAlert = "oklch(61.45% 0.24 30.12)", // rgba(243, 32, 12, 1)
  TextWhite = "oklch(98.51% 0 0)", // rgba(250, 250, 250, 1)
}

export interface IColorText {
  Transparent: PaletteColorText.Transparent;
  primary: PaletteColorText.TextPrimary;
  secondary: PaletteColorText.TextSecondary;
  accent: PaletteColorText.TextAccent;
  disabled: PaletteColorText.TextDisabled;
  success: PaletteColorText.TextSuccess;
  alert: PaletteColorText.TextAlert;
  white: PaletteColorText.TextWhite;
}

// Палитра цветов для заднего фона (background)
export enum PaletteColorBackground {
  Transparent = "transparent",
  BackgroundPrimary = "oklch(98.51% 0 0)", // rgba(250, 250, 250, 1)
  BackgroundAccent = "oklch(45.2% 0.2655745880837794 264.052020638055)", // rgba(0, 0, 255, 1)
  BackgroundSecondary = "oklch(93.52% 0.005 286.3)", // rgb(233.04, 233.04, 236.77)
  BackgroundWhite = "oklch(96.77% 0.003 286.35)", // rgba(244, 244, 246, 1)
}

export interface IColorBackground {
  Transparent: PaletteColorBackground.Transparent;
  primary: PaletteColorBackground.BackgroundPrimary;
  accent: PaletteColorBackground.BackgroundAccent;
  secondary: PaletteColorBackground.BackgroundSecondary;
  white: PaletteColorBackground.BackgroundWhite;
}

// Общая палитра TODO: понять зачем она нужна
export const PaletteColor = { ...PaletteColorText, ...PaletteColorBackground };
export type PaletteColor = typeof PaletteColor;
