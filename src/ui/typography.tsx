import styled, { css } from "styled-components"
import { getBoxStyles } from "./box"
import { colors } from "./colors"

interface ITypographySizes {
  fontWeight: number
  fontSize: number
  lineHeight: number
}

interface ITypographyMixin {
  size: ITypographySizes
  family: string
  color: string
}

export interface ITextBlock extends ITypographyMixin {
  renderTag: string
  children: React.ReactNode
}

export const typography = {
  sizes: {
    huge: {
      fontWeight: 400,
      fontSize: 64,
      lineHeight: 64,
    },
    large: {
      fontWeight: 400,
      fontSize: 36,
      lineHeight: 40,
    },
    medium: {
      fontWeight: 400,
      fontSize: 24,
      lineHeight: 28,
    },
    default: {
      fontWeight: 400,
      fontSize: 16,
      lineHeight: 24,
    },
    small: {
      fontWeight: 400,
      fontSize: 12,
      lineHeight: 16,
    },
  },
  families: {
    NeueMachina: "Neue-Machina",
    Lucida: "Lucida",
  },
}

export const typographySizeMixIn = (size: ITypographySizes) => {
  return `
      font-weight: ${size.fontWeight};
      font-size: ${size.fontSize}px;
      line-height: ${size.lineHeight}px;
    `
}

export const typographyFamilyMixIn = (family: string) => {
  return `
        font-family: ${family}, -apple-system, system-ui, BlinkMacSystemFont,
        "Helvetica Neue", sans-serif;
      `
}

export const typographyColorMixIn = (color: string) => {
  const selectedColor = () => {
    switch (color) {
      case "transparent": {
        return colors.text.transparent
      }
      case "primary": {
        return colors.text.primary
      }
      case "secondary": {
        return colors.text.secondary
      }
      case "accent": {
        return colors.text.accent
      }
      case "disabled": {
        return colors.text.disabled
      }
      default: {
        return color
      }
    }
  }
  const colorToApply = selectedColor()
  return `color: ${colorToApply}; fill: ${colorToApply}`
}

export const typographyMixin = ({
  size,
  family,
  color,
}: ITypographyMixin) => css`
  margin: 0;
  padding: 0;
  border: 0;
  vertical-align: baseline;
  word-break: break-word;
  ${() => typographySizeMixIn(size)}
  ${() => typographyFamilyMixIn(family)}
  ${() => typographyColorMixIn(color)} 
  ${() => getBoxStyles({})}
`

export const P = styled.p<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const Span = styled.span<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const H1 = styled.h1<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const H2 = styled.h2<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const H3 = styled.h3<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const H4 = styled.h4<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const H5 = styled.h5<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const H6 = styled.h6<ITypographyMixin>`
  ${(props) => typographyMixin(props)}
`

export const TextBlock = ({
  size = typography.sizes.default,
  family = typography.families.Lucida,
  color = colors.text.primary,
  renderTag = "span",
  children,
  ...props
}: ITextBlock) => {
  switch (renderTag) {
    case "p": {
      return (
        <P {...props} size={size} family={family} color={color}>
          {children}
        </P>
      )
    }
    case "h1":
      return (
        <H1 {...props} size={size} family={family} color={color}>
          {children}
        </H1>
      )
    case "h2":
      return (
        <H2 {...props} size={size} family={family} color={color}>
          {children}
        </H2>
      )
    case "h3":
      return (
        <H3 {...props} size={size} family={family} color={color}>
          {children}
        </H3>
      )
    case "h4":
      return (
        <H4 {...props} size={size} family={family} color={color}>
          {children}
        </H4>
      )
    case "h5":
      return (
        <H5 {...props} size={size} family={family} color={color}>
          {children}
        </H5>
      )
    case "h6":
      return (
        <H6 {...props} size={size} family={family} color={color}>
          {children}
        </H6>
      )
    case "span": {
      return (
        <Span {...props} size={size} family={family} color={color}>
          {children}
        </Span>
      )
    }
    case "Header1": {
      return (
        <H1
          {...props}
          size={typography.sizes.huge}
          family={typography.families.NeueMachina}
          color={color}
        >
          {children}
        </H1>
      )
    }
    case "Header2": {
      return (
        <H2
          {...props}
          size={typography.sizes.large}
          family={typography.families.NeueMachina}
          color={color}
        >
          {children}
        </H2>
      )
    }
    case "Headline": {
      return (
        <Span
          {...props}
          size={typography.sizes.default}
          family={typography.families.NeueMachina}
          color={color}
        >
          {children}
        </Span>
      )
    }
    case "Text": {
      return (
        <H3
          {...props}
          size={typography.sizes.medium}
          family={typography.families.NeueMachina}
          color={color}
        >
          {children}
        </H3>
      )
    }
    default: {
      return (
        <P {...props} size={size} family={family} color={color}>
          {children}
        </P>
      )
    }
  }
}
