import { typography } from "./src/ui/typography"

export interface IIconProps {
  color?: string
  size?: number
}
